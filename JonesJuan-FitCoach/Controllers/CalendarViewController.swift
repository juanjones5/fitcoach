//
//  CalendarViewController.swift
//  JonesJuan-FitCoach
//
//  Created by Juan Ignacio Jones on 3/11/19.
//  Copyright © 2019 DePaul University. All rights reserved.
//

import UIKit

class CalendarViewController: UIViewController {

    @IBOutlet weak var workoutName: UILabel!
    @IBOutlet weak var datePickerField: UITextField!
    @IBOutlet weak var goButton: UIButton!
    private var datePicker: UIDatePicker?
    var workoutIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "CALENDAR"
        goButton.isHidden = true
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(CalendarViewController.dateChanged(datePicker:)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CalendarViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        datePickerField.inputView = datePicker
        workoutName.text = ""
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        datePickerField.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
        for (index, workout) in workouts.enumerated() {
            if (Calendar.current.isDate(workout.date, inSameDayAs: datePicker.date)) {
                workoutName.text = workout.name
                workoutIndex = index
                goButton.isHidden = false
                return
            }
        }
        goButton.isHidden = true
        workoutName.text = "No Workout assigned"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let workoutDisplayViewController = segue.destination as? WorkoutDisplayViewController else { return }
        workoutDisplayViewController.workoutIndex = workoutIndex
    }


}
