//
//  ExerciseDetailViewController.swift
//  JonesJuan-FitCoach
//
//  Created by Juan Ignacio Jones on 3/15/19.
//  Copyright © 2019 DePaul University. All rights reserved.
//

import UIKit

class ExerciseDetailViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var shortLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionText: UITextView!
    var exercise : Exercise?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let e = exercise {
            nameLabel.text = e.name
            shortLabel.text = e.shortDescription
            descriptionText.text = e.instructions
            let url = URL(string: e.image)
            if let data = try? Data(contentsOf: url!) {
                imageView.image = UIImage(data: data)
            }
        }
    }
    

}
