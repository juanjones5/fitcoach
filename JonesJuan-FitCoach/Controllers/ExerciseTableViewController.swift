//
//  ExerciseTableViewController.swift
//  JonesJuan-FitCoach
//
//  Created by Juan Ignacio Jones on 3/11/19.
//  Copyright © 2019 DePaul University. All rights reserved.
//

import UIKit

class ExerciseTableViewController: UITableViewController {
    
    var stationIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        self.view.addGestureRecognizer(longPressRecognizer)
    }

    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {
        
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: self.tableView)
            if let indexPath = self.tableView.indexPathForRow(at: touchPoint) {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let detailViewController = storyBoard.instantiateViewController(withIdentifier: "detail") as! ExerciseDetailViewController
                let backItem = UIBarButtonItem()
                backItem.title = "Back"
                backItem.tintColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
                navigationItem.backBarButtonItem = backItem
                detailViewController.exercise = exercises[indexPath.row]
            self.navigationController?.pushViewController(detailViewController, animated: true)
            }
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if exercises.count == 0 {
            let emptyLabel = UILabel()
            emptyLabel.text = "No exercises available"
            emptyLabel.numberOfLines = 0
            emptyLabel.sizeToFit()
            emptyLabel.textAlignment = .center
            emptyLabel.backgroundColor = UIColor.lightGray
            emptyLabel.textColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
            tableView.separatorStyle = .none
            tableView.backgroundView = emptyLabel
        } else {
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
        }
        return exercises.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = exercises[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.type.rawValue, for: indexPath)
        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = item.shortDescription
        if workoutInProgres.stations[stationIndex].exercises.contains(where: { $0 === exercises[indexPath.row] }) {
            cell.accessoryType = .checkmark
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.cellForRow(at: indexPath)?.accessoryType == .checkmark {
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }
        else {
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            workoutInProgres.stations[stationIndex].exercises.append(exercises[indexPath.row])
        }
    }

}
