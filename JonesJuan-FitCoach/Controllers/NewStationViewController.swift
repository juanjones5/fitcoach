//
//  NewWorkoutViewController.swift
//  JonesJuan-FitCoach
//
//  Created by Juan Ignacio Jones on 3/11/19.
//  Copyright © 2019 DePaul University. All rights reserved.
//

import UIKit

class NewStationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var repsField: UITextField!
    @IBOutlet weak var setsField: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    
    var index = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        nameLabel.text = "Editing " + workoutInProgres.stations[index].name
        repsField.backgroundColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
        setsField.backgroundColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(NewStationViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
        if let sets = setsField.text {
            workoutInProgres.stations[index].sets = Int(sets)!
        }
        if let seconds = repsField.text {
            workoutInProgres.stations[index].seconds = Int(seconds)!
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if workoutInProgres.stations[index].exercises.count == 0 {
            let emptyLabel = UILabel()
            emptyLabel.text = "No exercises added yet"
            emptyLabel.numberOfLines = 0
            emptyLabel.sizeToFit()
            emptyLabel.textAlignment = .center
            emptyLabel.backgroundColor = UIColor.lightGray
            emptyLabel.textColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
            tableView.separatorStyle = .none
            tableView.backgroundView = emptyLabel
        } else {
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
        }
        return workoutInProgres.stations[index].exercises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = workoutInProgres.stations[index].exercises[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.type.rawValue, for: indexPath)
        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = item.shortDescription
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            workoutInProgres.stations[index].exercises.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Add selected exercises"
        backItem.tintColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
        navigationItem.backBarButtonItem = backItem
        guard let exerciseTableViewController = segue.destination as? ExerciseTableViewController else { return }
        exerciseTableViewController.stationIndex = index
        tableView.reloadData()
    }
    

}
