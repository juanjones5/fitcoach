//
//  FirstViewController.swift
//  JonesJuan-FitCoach
//
//  Created by Juan Ignacio Jones on 3/11/19.
//  Copyright © 2019 DePaul University. All rights reserved.
//

import UIKit

class WorkoutDisplayViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var workoutIndex = todaysWorkout()

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var createdByLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "WORKOUT VIEW"
        tableView.tableFooterView = UIView()
        if workoutIndex == -1 {
            todaysWorkout()
        }
        nameLabel.text = workouts[workoutIndex].name != "" ? workouts[workoutIndex].name : "No Workout assigned for this day"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy"
        dateLabel.text = dateFormatter.string(from: workouts[workoutIndex].date)
        createdByLabel.text = "by " + workouts[workoutIndex].createdBy
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        if workouts[workoutIndex].stations.count == 0 {
            let emptyLabel = UILabel()
            emptyLabel.text = "No stations in this workout"
            emptyLabel.numberOfLines = 0
            emptyLabel.sizeToFit()
            emptyLabel.textAlignment = .center
            emptyLabel.backgroundColor = UIColor.lightGray
            emptyLabel.textColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
            tableView.separatorStyle = .none
            tableView.backgroundView = emptyLabel
        } else {
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
        }
        return workouts[workoutIndex].stations.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.darkGray
        let label = UILabel()
        label.text = workouts[workoutIndex].stations[section].name
        label.textColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
        label.frame = CGRect(x: 15, y: 5, width: 100, height: 35)
        headerView.addSubview(label)
        let descriptionLabel = UILabel()
        descriptionLabel.text = workouts[workoutIndex].stations[section].sets.description + " Rounds | " + workouts[workoutIndex].stations[section].seconds.description + " Seconds each exercise"
        descriptionLabel.frame = CGRect(x: 15, y: 40, width: 300, height: 35)
        descriptionLabel.textColor = UIColor.lightGray
        headerView.addSubview(descriptionLabel)
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workouts[workoutIndex].stations[section].exercises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let category = workouts[workoutIndex].stations[indexPath.section]
        let item = category.exercises[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.type.rawValue, for: indexPath)
        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = item.shortDescription
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let exerciseDetailViewController = segue.destination as? ExerciseDetailViewController else { return }
        guard let cell = sender as? UITableViewCell else { return }
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        exerciseDetailViewController.exercise = workouts[workoutIndex].stations[indexPath.section].exercises[indexPath.row]
        tableView.reloadData()
    }
    
    

}

