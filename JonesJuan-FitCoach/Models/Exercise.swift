//
//  Exercise.swift
//  JonesJuan-FitCoach
//
//  Created by Juan Ignacio Jones on 3/11/19.
//  Copyright © 2019 DePaul University. All rights reserved.
//

import Foundation

var exercises = [
    Exercise(name: "Squat",
             type: .strength,
             shortDescription: "Leg exercise using your bodyweight",
             instructions: "Start standing with your feet slightly wider than hip-width apart, feet turned out about 5-15 degrees. Hinge your hips back toward a wall (real or imaginary) behind you, and bend your knees to lower into a squat. Keep the weight in your heels and your chest upright. Keep your knees inline with your feet (don't let them buckle in). Go as low as you can, then push through your heels to return to standing.",
             image: "https://hips.hearstapps.com/vidthumb/brightcove/578940a2e694aa370d88355b/thumb_1468612771.png?fill=2:1&resize=640:*"
    ),
    Exercise(name: "Deadlift",
             type: .strength,
             shortDescription: "Best lift for building total-body strength",
             instructions: "The deadlift is a weight training exercise in which a loaded barbell or bar is lifted off the ground to the level of the hips, then lowered to the ground.",
             image: "https://hips.hearstapps.com/vidthumb/brightcove/578940b6e694aa370d88358c/thumb_1468612792.png?fill=2:1&resize=640:*"
    ),
    Exercise(name: "Bench Press",
             type: .strength,
             shortDescription: "Upper-body strength-training exercise",
             instructions: "The bench press is a core fundamental exercise for developing upper body strength. You're not only working your pectorals (chest), you are also working your anterior deltoids (front shoulders), triceps brachii, and latissimus dorsi (back).",
             image: "https://3.bp.blogspot.com/-azSMBPoXJfg/WK987uyLRNI/AAAAAAAAFic/QvXmiV6iOScX3wmKzstFtTmce4f0tVjJwCLcB/s1600/2.png"
    ),
    Exercise(name: "Running",
             type: .cardio,
             shortDescription: "The most famous cardio exercise",
             instructions: "Try running at a steady pace for 10 minutes to warm up. Next, run up a hill for 30 seconds as fast as you can.",
             image: "https://xpressfitnessrunning.files.wordpress.com/2017/12/24132099_1572832929453175_1676592924154482584_o.jpg?w=400&h=200&crop=1"
    ),
    Exercise(name: "Rowing",
             type: .cardio,
             shortDescription: "Fun cardio workout",
             instructions: "Perhaps one of the best rowing machine benefits is the workout they give the lower body. In fact, rowing enthusiasts consider rowing primarily a lower-body workout. The main leg muscles involved are the quads in the upper front of the thighs",
             image: "https://bloximages.chicago2.vip.townnews.com/saratogian.com/content/tncms/assets/v3/editorial/0/01/001e652c-b367-506a-b79c-fe5d5af21282/5b9abc8ae844f.image.jpg?resize=400%2C200"
    ),
    Exercise(name: "Hamstring Stretch",
             type: .stretch,
             shortDescription: "Great exercise for your hamstrings",
             instructions: "Lie flat on your back and then lift your left leg into the air, so it is 90 degrees to the floor. Keeping your knee slightly bent, put your hand around your hamstring or calf and pull your leg towards your chest.",
             image: "https://usadojo.com/wp-content/uploads/2013/10/Lunge2.jpg"
    ),
    Exercise(name: "Windshield Wiper Twist",
             type: .stretch,
             shortDescription: "Good for your hips.",
             instructions: "Lay on your back on the floor and spread your feet slightly wider than hip-distance width apart. Outstretch your arms so your torso forms a “T.” Gently lower your left knee to your right ankle. Return to center, then gently lower your right knee to your left ankle. Repeat 5 times.",
             image: "https://hips.hearstapps.com/vidthumb/brightcove/57894013e694aa370d8833b9/thumb_1468612629.png?fill=2:1&resize=640:*"
    ),
    Exercise(name: "Leg Press",
             type: .strength,
             shortDescription: "Great leg exercise",
             instructions: "The leg press is performed while seated by pushing a weight away from the body with the feet. It is a compound exercise that also involves the glutes and, to a lesser extent, the hamstrings and the calves. Overloading the machine can result in serious injury if the sled moves uncontrollably towards the trainer.",
             image: "https://i1.wp.com/primofitnessusa.com/wp-content/uploads/2019/01/Panatta-SEC-Horizontal-Leg-Press-1SC085-Training.jpg?fit=400%2C200&ssl=1"
    ),
    Exercise(name: "Wall sit",
             type: .strength,
             shortDescription: "Over a flat vertical surface",
             instructions: "The wall sit, also known as a static squat, is performed by placing one's back against a wall with feet shoulder width apart, and lowering the hips until the knees and hips are both at right angles. The position is held as long as possible. The exercise is used to strengthen the quadriceps. Contrary to previous advice in this section, this exercise is NOT good for people with knee problems because the knees bear most of the load, especially when they are held at right angles (90 degrees).",
             image: "http://www.alive.com/images/2013/10-372/372-fit-wallangels.jpg"
    ),
    Exercise(name: "Spinal Twist",
             type: .stretch,
             shortDescription: "This exercise is good for your torso",
             instructions: "Start by laying on the floor. While keeping your left leg straight, bring your right knee into your chest. On the exhale, drop your right knee across your body. Then extend your right arm and look out over your right shoulder. Hold for 30-60 seconds. Then do the other side.",
             image: "https://cf.girlsaskguys.com/a26691/0b23a454-6305-4ba6-9869-79b89b0b98c1-m.jpg"
    ),
    Exercise(name: "Snatch",
             type: .cardio,
             shortDescription: "Current olympic weightlifting",
             instructions: "The essence of the event is to lift a barbell from the platform to locked arms overhead in a smooth continuous movement. The barbell is pulled as high as the lifter can manage (typically to mid [ chest] height) (the pull) at which point the barbell is flipped overhead. With relatively light weights (as in the power snatch) locking of the arms may not require rebending the knees. However, as performed in contests, the weight is always heavy enough to demand that the lifter receive the bar in a squatting position, while at the same time flipping the weight so it moves in an arc directly overhead to locked arms. When the lifter is secure in this position, he rises (overhead squat), completing the lift.",
             image: "https://www.guamsportsnetwork.com/wp-content/uploads/2017/07/IMG_3882-400x200.jpg"
    ),
    Exercise(name: "Swimming",
             type: .cardio,
             shortDescription: "Great for burning a lot of calories",
             instructions: "You should plan on swimming for 30 minutes, then, so that your actual exercise time (as opposed to rest time) ends up around 20 minutes. To begin, commit yourself to three times a week, 30 minutes per workout. Try swimming for as much of that time as you can, and count your laps.",
             image: "https://bloximages.newyork1.vip.townnews.com/hickoryrecord.com/content/tncms/assets/v3/editorial/d/b2/db28dec4-2d6d-11e9-b8a1-ebb35357e168/5c6081c24be03.image.jpg?resize=400%2C200"
    ),
    Exercise(name: "Foot and Arch Stretch",
             type: .stretch,
             shortDescription: "Good after biking for a while",
             instructions: "Grab a golf ball, tennis ball, or lacrosse ball, baseball. Place it under the arch of your foot, apply light to moderate pressure, and roll the ball around under your arch.",
             image: "https://bloximages.newyork1.vip.townnews.com/hickoryrecord.com/content/tncms/assets/v3/editorial/d/b2/db28dec4-2d6d-11e9-b8a1-ebb35357e168/5c6081c24be03.image.jpg?resize=400%2C200"
    )
]

func getData() {
    exercises.append(Exercise(name: "Gordo", type: .cardio, shortDescription: "corre gorod", instructions: "gordoooooo", image: "https://cdn1.coachmag.co.uk/sites/coachmag/files/styles/16x9_746/public/2018/03/home-dumbbell-workout-plan.jpg?itok=sujyP-IW&timestamp=1520599000"))
}


class Exercise {
    
    enum `Type`: String {
        case cardio = "cardio"
        case strength = "strength"
        case stretch = "stretch"
    }
    
    var name: String
    var type: Type
    var shortDescription: String
    var instructions: String
    var image: String
    
    init(name: String, type: Type, shortDescription: String, instructions: String, image: String) {
        self.name = name
        self.type = type
        self.shortDescription = shortDescription
        self.instructions = instructions
        self.image = image
    }
    
    init(dictionary: [String:String]) {
        self.name = dictionary["name"] ?? ""
        self.type = .cardio
        self.shortDescription = dictionary["shortDescription"] ?? ""
        self.instructions = dictionary["instructions"] ?? ""
        self.image = dictionary["image"] ?? ""
    }
    
}
