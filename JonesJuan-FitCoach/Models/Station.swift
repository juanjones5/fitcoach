//
//  Station.swift
//  JonesJuan-FitCoach
//
//  Created by Juan Ignacio Jones on 3/11/19.
//  Copyright © 2019 DePaul University. All rights reserved.
//

import Foundation

class Station {
    
    var name: String
    var exercises: [Exercise]
    var sets: Int
    var seconds: Int
    
    init(name: String, sets: Int, seconds: Int, exercises: [Exercise]) {
        self.name = name
        self.sets = sets
        self.seconds = seconds
        self.exercises = exercises
    }
}
