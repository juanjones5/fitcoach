//
//  Workout.swift
//  JonesJuan-FitCoach
//
//  Created by Juan Ignacio Jones on 3/11/19.
//  Copyright © 2019 DePaul University. All rights reserved.
//

import Foundation

var workoutInProgres = Workout()

var workouts = [
    Workout(
        name: "Burning Calories",
        createdBy: "Coach Juan",
        date: Date(),
        stations: [
            Station(name: "Station 1", sets: 3, seconds: 30, exercises: [exercises[0], exercises[1]]),
            Station(name: "Station 2", sets: 2, seconds: 30, exercises: [exercises[3], exercises[2]]),
            Station(name: "Station 3", sets: 4, seconds: 40, exercises: [exercises[5], exercises[6]]),
        ]
    )
]

func todaysWorkout() -> Int {
    for (index, workout) in workouts.enumerated() {
        if (Calendar.current.isDate(workout.date, inSameDayAs: Date())) {
            return index
        }
    }
    return 0
}


class Workout {
    var name: String
    var createdBy: String
    var date: Date
    var stations: [Station]
    
    
    init(name: String, createdBy: String, date: Date, stations: [Station]) {
        self.name = name
        self.createdBy = createdBy
        self.date = date
        self.stations = stations
    }
    
    init() {
        self.name = ""
        self.createdBy = ""
        self.date = Date()
        self.stations = [Station]()
    }
    
    
}
