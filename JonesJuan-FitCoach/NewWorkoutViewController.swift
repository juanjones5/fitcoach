//
//  SecondViewController.swift
//  JonesJuan-FitCoach
//
//  Created by Juan Ignacio Jones on 3/11/19.
//  Copyright © 2019 DePaul University. All rights reserved.
//

import UIKit

class NewWorkoutViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var createdByField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    private var datePicker: UIDatePicker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "NEW WORKOUT"
        tableView.tableFooterView = UIView()
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(CalendarViewController.dateChanged(datePicker:)), for: .valueChanged)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CalendarViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        dateField.inputView = datePicker
        nameField.backgroundColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
        createdByField.backgroundColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
        dateField.backgroundColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    
    @IBAction func save(_ sender: UIButton) {
        if workoutValid() {
            workoutInProgres.name = (nameField?.text)!
            workoutInProgres.createdBy = (createdByField.text)!
            workoutInProgres.date = (datePicker?.date)!
            workouts.append(workoutInProgres)
            let messageString = workoutInProgres.name + " has been assigned to the chosen date"
            reset()
            popUpMessage(title: "Workout Created", message: messageString)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if workoutInProgres.stations.count == 0 {
            let emptyLabel = UILabel()
            emptyLabel.text = "No stations added yet"
            emptyLabel.numberOfLines = 0
            emptyLabel.sizeToFit()
            emptyLabel.textAlignment = .center
            emptyLabel.backgroundColor = UIColor.lightGray
            emptyLabel.textColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
            tableView.separatorStyle = .none
            tableView.backgroundView = emptyLabel
        } else {
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
        }
        return workoutInProgres.stations.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.darkGray
        let label = UILabel()
        label.text = workoutInProgres.stations[section].name
        label.textColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
        label.frame = CGRect(x: 15, y: 5, width: 100, height: 35)
        headerView.addSubview(label)
        let descriptionLabel = UILabel()
        descriptionLabel.text = workoutInProgres.stations[section].sets.description + " Rounds | " + workoutInProgres.stations[section].seconds.description + " Seconds each exercise"
        descriptionLabel.frame = CGRect(x: 15, y: 40, width: 300, height: 35)
        descriptionLabel.textColor = UIColor.lightGray
        headerView.addSubview(descriptionLabel)
        return headerView

    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workoutInProgres.stations[section].exercises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let category = workoutInProgres.stations[indexPath.section]
        let item = category.exercises[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.type.rawValue, for: indexPath)
        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = item.shortDescription
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let count = workoutInProgres.stations[indexPath.section].exercises.count
            if count == 1 {
                workoutInProgres.stations.remove(at: indexPath.section)
                for (index, station) in workoutInProgres.stations.enumerated() {
                    station.name = "Station " + (index + 1).description
                }
                tableView.reloadData()
            }
            else {
                workoutInProgres.stations[indexPath.section].exercises.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        }
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Add this station"
        backItem.tintColor = UIColor(red:0.53, green:0.98, blue:0.81, alpha:1.0)
        navigationItem.backBarButtonItem = backItem
        guard let newStationViewController = segue.destination as? NewStationViewController else { return }
        let nameString = "Station " + (workoutInProgres.stations.count + 1).description
        workoutInProgres.stations.append(Station(name: nameString, sets: 0, seconds: 0, exercises: [Exercise]()))
        newStationViewController.index = workoutInProgres.stations.count - 1
        tableView.reloadData()
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        for workout in workouts {
            if (Calendar.current.isDate(workout.date, inSameDayAs: datePicker.date)) {
                popUpMessage(title: "Error", message: "There is already a workout assigned to this date")
                return
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateField.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    func popUpMessage(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okayAction)
        present(alertController, animated: true, completion: nil)
    }
    @IBAction func cancelAction(_ sender: UIButton) {
        reset()
    }
    
    func reset() {
        nameField.text = ""
        createdByField.text = ""
        dateField.text = ""
        workoutInProgres = Workout()
        tableView.reloadData()
    }
    
    
    func workoutValid() -> Bool {
        if nameField.text == "" || dateField.text == "" || createdByField.text == "" {
            popUpMessage(title: "Error", message: "Fields missing")
            return false
        }
        return true
    }
    

    



}

